window.env = process.env;

export default {
  get urlApi() {
    return window.env.HOST;
  },
};
