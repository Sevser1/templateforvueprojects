import Moment from 'moment';
import { extendMoment } from 'moment-range';

const moment = extendMoment(Moment);

export const formatDate = (date, format = 'DD.MM.YYYY') => {
  if (!date) {
    return '';
  }
  const dateFormatted = moment(date);
  return dateFormatted.isValid() ? dateFormatted.format(format) : '';
};

export const fromFormattedDate = (date, format = 'DD.MM.YYYY') => {
  const dateFormatted = moment(date, format);
  return dateFormatted.isValid() ? dateFormatted.toDate() : undefined;
};

export const formatDateTime = (date, format = 'DD.MM.YYYY HH:mm') => {
  const dateFormatted = moment(date);
  return dateFormatted.isValid() ? dateFormatted.format(format) : '';
};

export const formatYesNo = value => (value ? 'Да' : 'Нет');

export const formatMoney = value => parseFloat(value).toLocaleString('ru', { style: 'currency', currency: 'RUB' });
