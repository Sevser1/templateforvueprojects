/* eslint class-methods-use-this: ["error", { "exceptMethods": ["setRefreshToken", "setAccessToken", "getCurrentStorage", "clear", "getAccessToken", "getRefreshToken"] }] */
import Vue from 'vue';

class LocalStorageManager {
  constructor() {
    this.eventBus = new Vue();
  }

  $on(a, b) {
    return this.eventBus.$on(a, b);
  }

  getCurrentStorage() {
    const state = localStorage.getItem('state');
    if (state) {
      return JSON.parse(state);
    }
    return {};
  }

  updateStorage(newState = {}) {
    const state = {
      ...this.getCurrentStorage(),
      ...newState,
    };
    localStorage.setItem('state', JSON.stringify(state));
    return this;
  }

  clear() {
    localStorage.removeItem('state');
  }

  getAccessToken() {
    return localStorage.getItem('tAccess');
  }

  getRefreshToken() {
    return localStorage.getItem('tRefresh');
  }

  getUserEmail() {
    return localStorage.getItem('email');
  }

  setAccessToken(token) {
    localStorage.setItem('tAccess', token);
    this.eventBus.$emit('set:tAccess');
    return this;
  }

  setRefreshToken(token) {
    localStorage.setItem('tRefresh', token);
    this.eventBus.$emit('set:tRefresh');
    return this;
  }

  setUserEmail(email) {
    localStorage.setItem('email', email);
    return this;
  }

  removeTokens() {
    localStorage.removeItem('tAccess');
    localStorage.removeItem('tRefresh');
    this.eventBus.$emit('remove:tAccess');
    this.eventBus.$emit('remove:tRefresh');
    return this;
  }

  removeUserEmail() {
    localStorage.removeItem('email');
    return this;
  }
}

if (!window.LocalStorageManager) {
  window.LocalStorageManager = new LocalStorageManager();
}

export default window.LocalStorageManager;
