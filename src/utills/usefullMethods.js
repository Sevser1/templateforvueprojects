export const clone = param => JSON.parse(JSON.stringify(param));

export const addPrefixToKeys = (obj, prefix) =>
  Object.keys(obj).reduce((acc, key) => ({ ...acc, [`${prefix}/${key}`]: obj[key] }), {});
